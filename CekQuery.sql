SELECT 
	p.name, 
	p.secure_id, 
	SUM(CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END) AS total 
FROM records r 
INNER JOIN receivable_detail rc 
ON r.id = rc.record_id 
INNER JOIN persons p ON p.id = rc.person_id 
LEFT JOIN ledgers l ON l.record_id = r.id
GROUP BY p.name, p.secure_id
ORDER BY r.record_date

SELECT 
	p.name,
	p.secure_id,
	SUM(CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END) AS total
FROM persons p
INNER JOIN receivable_detail rc
	ON p.id = rc.person_id
INNER JOIN records r ON r.id = rc.record_id 
LEFT JOIN ledgers l ON l.record_id = r.id
GROUP BY p.name, p.secure_id;

SELECT 
	p.name,
	p.secure_id,
	SUM(CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END) AS total
FROM persons p
INNER JOIN receivable_detail rc
	ON p.id = rc.person_id
INNER JOIN records r ON r.id = rc.record_id 
LEFT JOIN ledgers l ON l.record_id = r.id
GROUP BY p.name, p.secure_id;

SELECT * FROM records
