package com.nael.finnoteapi.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.nael.finnoteapi.DTOs.auth.RequestLogin;
import com.nael.finnoteapi.DTOs.auth.RequestRegistration;
import com.nael.finnoteapi.DTOs.auth.ResponseJwt;
import com.nael.finnoteapi.entities.Role;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.ERole;
import com.nael.finnoteapi.repositories.RoleRepository;
import com.nael.finnoteapi.repositories.UserRepository;
import com.nael.finnoteapi.utils.JwtUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AuthService {
	
	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	RoleRepository roleRepository;
	
	@Autowired
	PasswordEncoder encoder;
	
	@Autowired
	JwtUtils jwtUtils;
	
	public ResponseJwt authenticate(RequestLogin request) {
//		TODO: bikin implementasi authentication manager
		Authentication authentication = (UsernamePasswordAuthenticationToken) authenticationManager.authenticate(
					new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword())
				);
		log.info("AuthService.authenticate");
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		
		UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		
		String jwt = jwtUtils.generateJwtToken(authentication);
		
		List<String> roles = userDetails.getAuthorities().stream()
				.map(auth -> auth.getAuthority()).collect(Collectors.toList());
		
		return ResponseJwt.builder()
				.token(jwt)
				.secureId(userDetails.getSecureId())
				.email(userDetails.getUsername())
				.roles(roles)
				.build();
	}
	
	public boolean registerUser(RequestRegistration request) {
		User user = User.builder()
				.email(request.getEmail())
				.fullName(request.getFullName())
				.passwordHash(encoder.encode(request.getPassword()))
				.build();
		
		List<Role> roles = new ArrayList<>();
		
		Role userRole = roleRepository.findByName(ERole.ROLE_USER)
				.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
		roles.add(userRole);
		
		user.setRoles(roles.stream().toList());
		
		userRepository.save(user);
		
		return true;
	}
	
}
