package com.nael.finnoteapi.services;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;
import com.nael.finnoteapi.entities.Record;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import com.nael.finnoteapi.repositories.RecordRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class RecordService {
	
	private final RecordRepository recordRepository;
	
	public void softDeleteRecord(String secureId) {
		try {
			Optional<Record> optRecord = recordRepository.findBySecureId(secureId);
			if(optRecord.isEmpty()) {
				throw new NotFoundException("Data tidak ditemukan");
			}
			Record resultRecord = optRecord.get();
			var utcDate = new Date(Instant.now().toEpochMilli());
			resultRecord.setDeletedAt(utcDate);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	
}
