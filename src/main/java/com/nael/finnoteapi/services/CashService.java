package com.nael.finnoteapi.services;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nael.finnoteapi.DTOs.app.person.ResponsePerson;
import com.nael.finnoteapi.DTOs.app.record.RequestDateRange;
import com.nael.finnoteapi.DTOs.app.record.RequestNewCashRecord;
import com.nael.finnoteapi.DTOs.app.record.ResponseCashBalanceSum;
import com.nael.finnoteapi.DTOs.app.record.ResponseCashRecord;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.Record;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.enums.RecordGroup;
import com.nael.finnoteapi.repositories.LedgerRepository;
import com.nael.finnoteapi.repositories.RecordRepository;
import com.nael.finnoteapi.utils.PaginationUtil;

import jakarta.transaction.Transactional;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CashService {

	private final RecordRepository recordRepository;
	
	private final LedgerRepository ledgerRepository;
	
	private final UserDetailsImpl userDetails;
	
	@Autowired
	public CashService(RecordRepository recordRepository, LedgerRepository ledgerRepository, UserDetailsImpl userDetails) {
		super();
		this.recordRepository = recordRepository;
		this.ledgerRepository = ledgerRepository;
		this.userDetails = userDetails;
	}
	
//	TODO: Lanjut bikin service untuk sum total kas
	public ResponseCashBalanceSum sumCashBalancePerUser() {
		try {
			log.info("userDetails.getId: {}", userDetails.getId());
			Long sumResult = ledgerRepository.sumLedgerBalance(userDetails.getId());
			return ResponseCashBalanceSum.builder()
					.totalBalance(sumResult)
					.userId(userDetails.getSecureId())
					.build();
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponseCashRecord> findAllCashRecord(
			Integer page, Integer limit, 
			String sortBy, String direction, 
			String keyword, Date startDate, Date endDate
	) {
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(direction), sortBy));
			limit = limit > 1000 ? 1000 : limit;
			Pageable pageable = PageRequest.of(page, limit, sort);
//			FIXME: ternyata kita nggak bisa pasang sortedFieldAware di response,jadi konyol
//			coba pikirin cara lain biar fieldSortednya bisa terkendali dan tervalidasi
			if(startDate == null || endDate == null) {
				startDate = new Date(0);
				endDate = new Date();
			}
			
			Page<ResponseCashRecord> pageResult = ledgerRepository
					.getCashRecordPageable(userDetails.getId(), startDate, endDate, pageable);
			List<ResponseCashRecord> recordList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<ResponseCashRecord>(
						recordList, pageResult.getTotalPages(), pageResult.getTotalElements(), "", page
					);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@Transactional
	public void insertInRecord(RequestNewCashRecord dto) {
		try {
			User user = User.builder()
					.id(userDetails.getId())
					.build();
			
//			Save record
			RecordGroup recordGroup = dto.getEntry().equals(Entry.DEBIT) ? RecordGroup.CASH_IN : RecordGroup.CASH_OUT;
			
			Record newRecord = Record.builder()
					.recordGroup(recordGroup)
					.user(user)
					.description(dto.getDescription())
					.build();
			
			var utcDate = new Date(Instant.now().toEpochMilli());
			newRecord.setCreatedAt(utcDate);
			newRecord.setUser(user);
			recordRepository.save(newRecord);
			
//			Save Ledger
			var newLedger = Ledger.builder()
					.record(newRecord)
					.account(Account.CASH)
					.entry(dto.getEntry())
					.balance(dto.getBalance())
					.build();
//			TODO: Lanjut bikin endpoint untuk new cash record
			newLedger.setCreatedAt(utcDate);
			ledgerRepository.save(newLedger);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
}
