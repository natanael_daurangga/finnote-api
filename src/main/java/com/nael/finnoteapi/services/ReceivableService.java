package com.nael.finnoteapi.services;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.antlr.v4.runtime.UnbufferedTokenStream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import com.nael.finnoteapi.DTOs.app.record.RequestNewReceivable;
import com.nael.finnoteapi.DTOs.app.record.ResponseReceivableBalanceSum;
import com.nael.finnoteapi.DTOs.app.record.ResponseRecordSummary;
import com.nael.finnoteapi.DTOs.app.record.ResponseSumPerPerson;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.ReceivableDetail;
import com.nael.finnoteapi.entities.Record;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.enums.RecordGroup;
import com.nael.finnoteapi.repositories.LedgerRepository;
import com.nael.finnoteapi.repositories.PersonRepository;
import com.nael.finnoteapi.repositories.ReceivableRepository;
import com.nael.finnoteapi.repositories.RecordRepository;
import com.nael.finnoteapi.utils.PaginationUtil;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class ReceivableService {

	private final ReceivableRepository receivableRepository;
	
	private final LedgerRepository ledgerRepository;
	
	private final RecordRepository recordRepository;
	
	private final PersonRepository personRepository;
	
	private final UserDetailsImpl userDetails;
	
	@Transactional
	public void saveNewRecord(RequestNewReceivable dto) {
		try {
			User user = User.builder()
					.id(userDetails.getId())
					.build();
//			SAVE RECORD
			RecordGroup recordGroup = dto.getEntry().equals(Entry.DEBIT) ? RecordGroup.NEW_RECEIVABLE : RecordGroup.RECEIVABLE_PAYMENT;
			
			Record newRecord = Record.builder()
					.recordDate(dto.getTransactionDate())
					.recordGroup(recordGroup)
					.user(user)
					.description(dto.getDescription())
					.build();
			var utcDate = new Date(Instant.now().toEpochMilli());
			newRecord.setCreatedAt(utcDate);
			newRecord.setUser(user);
			
//			SAVE LEDGER
			var newLedger = Ledger.builder()
					.record(newRecord)
					.account(Account.RECEIVABLE)
					.entry(dto.getEntry())
					.balance(dto.getBalance())
					.build();
//			FIND DEBTOR
			Optional<Person> debtorOpt = personRepository.findOneBySecureIdAndUserId(dto.getDebtorSecureId(), userDetails.getId());
			if(debtorOpt.isEmpty()) throw new NotFoundException("Debtor tidak ditemukan");
			
//			SAVE RECEIVABLE DETAIL
			var newReceivableDetail = ReceivableDetail.builder()
					.debtor(debtorOpt.get())
					.record(newRecord)
					.build();
			
			recordRepository.save(newRecord);
			ledgerRepository.save(newLedger);
			receivableRepository.save(newReceivableDetail);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponseRecordSummary> paginateDetail(
			Integer page, Integer limit,
			String sortBy, String direction,
			String keyword, Date startDate, 
			Date endDate, String debtorSecureId
		){
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(direction), sortBy));
			limit = limit > 1000 ? 1000 : limit;
			Pageable pageable = PageRequest.of(page, limit, sort);
			
			if(startDate == null || endDate == null) {
				startDate = new Date(0);
				endDate = new Date();
			}
			
			Page<ResponseRecordSummary> pageResult = receivableRepository
					.getReceivableTransactionDetail(userDetails.getId(), debtorSecureId, startDate, endDate, pageable);
			List<ResponseRecordSummary> recordList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<ResponseRecordSummary>(
					recordList, pageResult.getTotalPages(), pageResult.getTotalElements(), "", page
					);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponseSumPerPerson> paginate(
				Integer page, Integer limit,
				String sortBy, String direction,
				String keyword, Date startDate, 
				Date endDate
			){
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(direction), sortBy));
			limit = limit > 1000 ? 1000 : limit;
			Pageable pageable = PageRequest.of(page, limit, sort);
			
			if(startDate == null || endDate == null) {
				startDate = new Date(0);
				endDate = new Date();
			}
			
			Page<ResponseSumPerPerson> pageResult = receivableRepository.getReceivablePerDebtor(userDetails.getId(), pageable);
			List<ResponseSumPerPerson> recordList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<ResponseSumPerPerson>(
						recordList, pageResult.getTotalPages(), pageResult.getTotalElements(), keyword, page
					);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponseReceivableBalanceSum receivableBalanceSum(String debtorSecureId) {
		try {
			Long sum = receivableRepository.sumReceivablePerDebtor(userDetails.getId(), debtorSecureId);
			return new ResponseReceivableBalanceSum(debtorSecureId, sum);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
}
