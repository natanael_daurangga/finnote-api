package com.nael.finnoteapi.services;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.nael.finnoteapi.DTOs.app.person.RequestPersonCreate;
import com.nael.finnoteapi.DTOs.app.person.ResponsePerson;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.repositories.PersonRepository;
import com.nael.finnoteapi.repositories.UserRepository;
import com.nael.finnoteapi.utils.PaginationUtil;

import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PersonService {

	private final PersonRepository personRepository;
	
	private final UserRepository userRepository;
	
	private final UserDetailsImpl userDetails;
	
	public void insertNewPerson(RequestPersonCreate dto) {
		try {
			var person = Person.builder()
					.email(dto.getEmail())
					.name(dto.getName())
					.address(dto.getAddress())
					.phoneNumber(dto.getPhoneNumber())
					.build();
			
			var utcDate = new Date(Instant.now().toEpochMilli());
			person.setCreatedAt(utcDate);
			
			var user = User.builder()
					.id(userDetails.getId())
					.build();
			
			person.setUser(user);
			
			personRepository.save(person);
		} catch (Exception e) {
			throw e;
		}
	}
	
	public void softDeletePerson(String secureId) {
		try {
			Optional<Person> optPerson = personRepository.findOneBySecureIdAndUserId(secureId, userDetails.getId());
			if(optPerson.isEmpty()) {
				log.info("person not exists");
				throw new EntityNotFoundException();
			}
			var utcDate = new Date(Instant.now().toEpochMilli());
			Person person = optPerson.get();
			log.info("person exists, secureId: {}", optPerson.get().getSecureId());
			person.setDeletedAt(utcDate);
			
			personRepository.save(person);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponsePerson> findAllPerson(Integer page, Integer limit, String sortBy,
			String direction, String keyword) {
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(direction), sortBy));
			limit = limit > 1000 ? 1000 : limit;
			Pageable pageable = PageRequest.of(page, limit, sort);
			Page<ResponsePerson> pageResult = personRepository.getPersonsPageable(userDetails.getId(), keyword, pageable);
			List<ResponsePerson> personList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<>(personList, pageResult.getTotalPages(), pageResult.getTotalElements(), keyword, page);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePerson findPerson(String secureId) {
		try {
			Optional<Person> personOpt = personRepository.findOneBySecureIdAndUserId(secureId, userDetails.getId());
			if(personOpt.isEmpty()) {
				throw new EntityNotFoundException();
			}
			
			Person person = personOpt.get();
			User user = new User();
			user.setSecureId(userDetails.getSecureId());
			person.setUser(user);
			
			return new ResponsePerson(person);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
}
