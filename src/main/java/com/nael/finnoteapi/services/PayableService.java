package com.nael.finnoteapi.services;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.nael.finnoteapi.DTOs.app.record.RequestNewPayable;
import com.nael.finnoteapi.DTOs.app.record.ResponsePayableBalanceSum;
import com.nael.finnoteapi.DTOs.app.record.ResponseRecordSummary;
import com.nael.finnoteapi.DTOs.app.record.ResponseSumPerPerson;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.PayableDetail;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.Record;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.enums.RecordGroup;
import com.nael.finnoteapi.repositories.LedgerRepository;
import com.nael.finnoteapi.repositories.PayableRepository;
import com.nael.finnoteapi.repositories.PersonRepository;
import com.nael.finnoteapi.repositories.RecordRepository;
import com.nael.finnoteapi.utils.PaginationUtil;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class PayableService {

	private final PayableRepository payableRepository;
	
	private final LedgerRepository ledgerRepository;
	
	private final RecordRepository recordRepository;
	
	private final PersonRepository personRepository;
	
	private final UserDetailsImpl userDetails;
	
	@Transactional
	public void saveNewRecord(RequestNewPayable dto) {
		try {
			User user = User.builder()
					.id(userDetails.getId())
					.build();
			
//			SAVE RECORD
			RecordGroup recordGroup = dto.getEntry().equals(Entry.CREDIT) ? RecordGroup.NEW_PAYABLE : RecordGroup.PAYABLE_PAYMENT;
			
			Record newRecord = Record.builder()
					.recordDate(dto.getTransactionDate())
					.recordGroup(recordGroup)
					.user(user)
					.description(dto.getDescription())
					.build();
			
			var utcDate = new Date(Instant.now().toEpochMilli());
			newRecord.setCreatedAt(utcDate);
			newRecord.setUser(user);
			
			var newLedger = Ledger.builder()
					.record(newRecord)
					.account(Account.PAYABLE)
					.entry(dto.getEntry())
					.balance(dto.getBalance())
					.build();
					
//			FIND CREDITOR
			Optional<Person> creditorOpt = personRepository
					.findOneBySecureIdAndUserId(dto.getCreditorSecureId(), userDetails.getId());
			
//			SAVE PAYABLE DETAIL
			var newPayableDetail = PayableDetail.builder()
					.creditor(creditorOpt.get())
					.record(newRecord)
					.build();
			
			recordRepository.save(newRecord);
			ledgerRepository.save(newLedger);
			payableRepository.save(newPayableDetail);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponseRecordSummary> paginateDetail(
				Integer page, Integer limit,
				String sortBy, String direction,
				String keyword, Date startDate,
				Date endDate, String debtorSecureId
			){
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(sortBy), sortBy));
			Pageable pageable = PageRequest.of(page, limit, sort);
			
			if(startDate == null || endDate == null ) {
				startDate = new Date(0);
				endDate = new Date();
			}
			
			Page<ResponseRecordSummary> pageResult = payableRepository
					.getPayablePerCreditor(userDetails.getId(), debtorSecureId, startDate, endDate, pageable);
			List<ResponseRecordSummary> recordList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<ResponseRecordSummary>(
						recordList, pageResult.getTotalPages(), pageResult.getTotalElements(), "", page
					);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePagination<ResponseSumPerPerson> paginate(
			Integer page, Integer limit,
			String sortBy, String direction,
			String keyword, Date startDate, 
			Date endDate
		){
		try {
			Sort sort = Sort.by(new Sort.Order(PaginationUtil.getDirection(direction), sortBy));
			limit = limit > 1000 ? 1000 : limit;
			Pageable pageable = PageRequest.of(page, limit, sort);
			
			if(startDate == null || endDate == null) {
				startDate = new Date(0);
				endDate = new Date();
			}
			
			Page<ResponseSumPerPerson> pageResult = payableRepository
					.getPayablePerDebtor(userDetails.getId(), pageable);
			List<ResponseSumPerPerson> recordList = pageResult.stream().map(b -> b).toList();
			return new ResponsePagination<ResponseSumPerPerson>(
						recordList, pageResult.getTotalPages(), pageResult.getTotalElements(), keyword, page
					);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	public ResponsePayableBalanceSum payableBalanceSum(String creditorSecureId) {
		try {
			Long sum = payableRepository.sumPayablePerCreditor(userDetails.getId(), creditorSecureId);
			return new ResponsePayableBalanceSum(creditorSecureId, sum);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
}
