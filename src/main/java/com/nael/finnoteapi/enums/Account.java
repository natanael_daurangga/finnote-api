package com.nael.finnoteapi.enums;

public enum Account {

	CASH,
	RECEIVABLE,
	PAYABLE,
	BAD_DEBT
	
}
