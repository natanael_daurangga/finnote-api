package com.nael.finnoteapi.enums;

public enum RecordGroup {
	CASH_IN,
	CASH_OUT,
	NEW_RECEIVABLE,
	NEW_PAYABLE,
	RECEIVABLE_PAYMENT,
	PAYABLE_PAYMENT
}
