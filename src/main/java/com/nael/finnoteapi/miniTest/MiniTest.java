package com.nael.finnoteapi.miniTest;

import java.io.File;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.TimeZone;

import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nael.finnoteapi.DTOs.util.SortedFieldAware;
import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.utils.DTOs.ReceivableListSeedDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedRequestDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedResponseDTO;
import com.nael.finnoteapi.utils.seeders.MainSeeder;
import com.nael.finnoteapi.utils.seeders.mapper.PersonSeedMapper;

import java.util.List;
import java.util.Map;

public class MiniTest {
	
	
	
	static class Car implements SortedFieldAware {
		private int tires = 2;
		
		public void setTires(int tires) {
			this.tires = tires;
		}
		
		public int getTires() {
			return tires;
		}

		@Override
		public String getDefaultField() {
			// TODO Auto-generated method stub
			return "test";
		}
	}

	public static void main(String[] args) throws Exception {
		ObjectMapper objMapper = new ObjectMapper();
		try {
			File file = ResourceUtils.getFile("classpath:json/test.json");
			String jsonExp = new String(Files.readAllBytes(file.toPath()));
//			List<LinkedHashMap<String, String>> personsMap = objMapper.readValue(file, LinkedList.class); // ternyata person itu jadi linkedhashmap
//			User user = User.builder()
//					.id(1l)
//					.email("el@tester.com")
//					.build();
//			List<String> persons = personsMap.stream().map(m -> {
//				Person person = Person.builder()
//						.user(user)
//						.email(m.get("halloo"))
//						.address(m.get("address"))
//						.name(m.get("name"))
//						.phoneNumber(m.get("phoneNumber"))
//						.build();
//				return person.toString();
//			}).toList();

//			if(personsMap != null && personsMap.size() > 0) {
//				System.out.println("Hurayy!!");
//				System.out.println("Length: " + personsMap.size());
//				System.out.println(persons);
//			} else {
//				System.out.println("coba lagi =)");
//			}
//			File file2 = ResourceUtils.getFile("classpath:json/receivableSeeds.json");
//			JsonNode receivableNodes = objMapper.readValue(file2, JsonNode.class);
//			for(JsonNode receivableNode : receivableNodes) {
//				System.out.println(receivableNode);
//				JsonNode personNode
//			}
			
//			JsonNode jsonNode = objMapper.readTree(file2);
//			ArrayNode arrayNode = (ArrayNode) jsonNode.get("receivables");
//			Map<String, String> maps = new HashMap<>();
//			for (JsonNode itemNode : arrayNode) {
//				List<JsonNode> ledgers = itemNode.findValues("records");
////				System.out.println(ledgers.size());
//				System.out.println(ledgers.get(0));
//			}
//			TODO: Lanjut bikin node
//			HashMap<String, List<HashMap<String, String>>> receivables = objMapper.readValue(file2, HashMap.class);
//			System.out.println(receivables);
//			Object obj = new Car(); 
//			Field field = obj.getClass().getDeclaredField("tires");
//			if(field != null) {
//				System.out.println(field.getName());				
//			} else {
//				System.out.println("Bakekok");
//			}
			
//			System.out.println(SortedFieldAware.class.isAssignableFrom(Class.class));
		} catch (Exception e) {
			throw e;
		}
	}
}
