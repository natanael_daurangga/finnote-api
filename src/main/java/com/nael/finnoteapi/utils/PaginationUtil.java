package com.nael.finnoteapi.utils;

import java.lang.reflect.Field;
import java.util.List;

import org.springframework.data.domain.Sort;

import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.DTOs.util.SortedFieldAware;

public class PaginationUtil {
	
	public static Sort.Direction getDirection(String direction) {
		if(direction.equalsIgnoreCase("asc")) {
			return Sort.Direction.ASC;
		} else {
			return Sort.Direction.DESC;
		}
	}
	
//	public static String validateSortedField(String sortedField, Class<?> dtoClass) {
//		if(!(object instanceof SortedFieldAware)) {
//			throw new IllegalArgumentException("Given object doesn't satisfy the requirements (? implements SortedFieldAware)");
//		}
//		
//		SortedFieldAware fieldAwareObj = (SortedFieldAware) object;
//		
//		try {
//			Field field = object.getClass().getDeclaredField(sortedField);
//			return sortedField;
//		} catch (Exception e) {
//			return fieldAwareObj.getDefaultField();
//		}
//	}
	
}
