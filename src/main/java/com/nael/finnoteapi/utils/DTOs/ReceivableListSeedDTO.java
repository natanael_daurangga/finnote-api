package com.nael.finnoteapi.utils.DTOs;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ReceivableListSeedDTO {

	List<ReceivableListSeedDTO> receivables;
}
