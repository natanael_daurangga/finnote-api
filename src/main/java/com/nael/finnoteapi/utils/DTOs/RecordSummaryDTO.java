package com.nael.finnoteapi.utils.DTOs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class RecordSummaryDTO {
	
	private long recordDate;
	
	private String description;
	
	private int account;
	
	private int entry;
	
	private long balance;
	
}
