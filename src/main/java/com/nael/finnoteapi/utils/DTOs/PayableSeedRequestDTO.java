package com.nael.finnoteapi.utils.DTOs;

import java.util.List;

import com.nael.finnoteapi.entities.Person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class PayableSeedRequestDTO {

	private Person person;
	
	private List<RecordSummaryDTO> records;
	
}
