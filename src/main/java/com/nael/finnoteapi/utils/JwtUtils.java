package com.nael.finnoteapi.utils;

import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.services.UserDetailsImpl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtParser;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class JwtUtils {
	
	@Value("${nael.app.jwtSecret}")
	private String jwtSecret;
	
	@Value("${nael.app.jwtExpiration}")
	private int jwtExpirationsMs;
	
	@Value("${nael.app.jwtAuthoritiesKey}")
	private String AUTHORITIES_KEY;
	
	@Value("${nael.app.userSecureId}")
	private String SECURE_ID;
	
	public String generateJwtToken(User user) {
//		2023-11-05, Nael : Menambahkan claim role di jwt tokennya
		List<String> rolesList = user.getRoles().stream()
				.map(role -> role.getName().name()).toList();
		final String rolesString = String.join(",", rolesList);
		Map<String, Object> extraClaims = new HashMap<>();
		extraClaims.put(AUTHORITIES_KEY, rolesString);
		extraClaims.put(SECURE_ID, user.getSecureId());
		
		return Jwts.builder()
				.setClaims(extraClaims)
				.setId(user.getId().toString())
				.setSubject((user.getEmail()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationsMs))
				.signWith(key(), SignatureAlgorithm.HS256)
				.compact();
	}
	
	@Deprecated
	public String generateJwtToken(Authentication authentication, Map<String, Object> extraClaims) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
//		TODO: tambahin juga role, id, dan email ke jwt tokennya, supaya kita nggak perlu lagi select dari database buat get user
		return Jwts.builder()
				.setClaims(extraClaims)
				.setId(userPrincipal.getId().toString())
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationsMs))
				.signWith(key(), SignatureAlgorithm.HS256)
				.compact();
	}
	
	public String generateJwtToken(Authentication authentication) {
		UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
		Map<String, Object> extraClaims = new HashMap<>();
		List<String> roles = userPrincipal.getAuthorities().stream()
				.map(role -> role.getAuthority()).toList();
		final String rolesString = String.join(",", roles);
		
		return Jwts.builder()
				.setClaims(extraClaims)
				.claim(AUTHORITIES_KEY, rolesString)
				.claim(SECURE_ID, userPrincipal.getSecureId())
				.setId(userPrincipal.getId().toString())
				.setSubject((userPrincipal.getUsername()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + jwtExpirationsMs))
				.signWith(key(), SignatureAlgorithm.HS256)
				.compact();
	}
	
	private Key key() {
		return Keys.hmacShaKeyFor(Decoders.BASE64.decode(jwtSecret));
	}
	
	public String getUsernameFromJwtToken(String token) {
		return Jwts.parserBuilder().setSigningKey(key()).build()
				.parseClaimsJws(token).getBody().getSubject();
	}
	
	public UsernamePasswordAuthenticationToken getAuthenticationFromToken(final String token) {
		Claims jwsClaims = Jwts.parserBuilder().setSigningKey(key()).build()
				.parseClaimsJws(token).getBody();
		final Long id = Long.parseLong(jwsClaims.getId());
		final String email = jwsClaims.getSubject();
		final String secureId = jwsClaims.get(SECURE_ID).toString();
		
		final Collection<? extends GrantedAuthority> authorities = 
				Arrays.stream(jwsClaims.get(AUTHORITIES_KEY)
				.toString().split(","))
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toList());
		
		final UserDetailsImpl userDetails = new UserDetailsImpl(id, email, null, authorities);
		userDetails.setSecureId(secureId);
		return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
	}
	
	
	
	public boolean validateJwtToken(String authToken) {
		try {
			log.info("key: {}", key());
			Jwts.parserBuilder().setSigningKey(key()).build().parse(authToken);
			return true;
		} catch (MalformedJwtException e) {
			log.error("Invalid JWT token: {}", e.getMessage());
	    } catch (ExpiredJwtException e) {
	    	log.error("JWT token is expired: {}", e.getMessage());
	    } catch (UnsupportedJwtException e) {
	    	log.error("JWT token is unsupported: {}", e.getMessage());
	    } catch (IllegalArgumentException e) {
	    	log.error("JWT claims string is empty: {}", e.getMessage());
	    }
		
	    return false;
	}
	
}
