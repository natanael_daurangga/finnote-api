package com.nael.finnoteapi.utils.seeders.mapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.User;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class PersonSeedMapper {
	
	private final ObjectMapper objMapper;
	
	public List<Person> jsonToObject(File file, List<User> users) throws Exception, FileNotFoundException {
		if(file == null || !file.exists()) throw new FileNotFoundException();
		String jsonExp = new String(Files.readAllBytes(file.toPath()));
		Random rand = new Random();
		try {
			List<LinkedHashMap<String, String>> streamMaps = objMapper.readValue(file, List.class);
			List<Person> result = streamMaps.stream().map(m -> {
				var utcDate = new Date(Instant.now().toEpochMilli());
				User user = users.get(rand.nextInt(users.size()));
				log.info("user exists {}", user.getFullName());
				Person person = Person.builder()
						.user(user)
						.email(m.get("email"))
						.address(m.get("address"))
						.name(m.get("name"))
						.phoneNumber(m.get("phoneNumber"))
						.build();
				person.setCreatedAt(utcDate);
				return person;
			}).toList();
			return result;
		} catch (Exception e) {
			throw e;
		}
	}
	
}

