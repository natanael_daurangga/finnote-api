package com.nael.finnoteapi.utils.seeders.mapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.utils.DTOs.PayableSeedRequestDTO;
import com.nael.finnoteapi.utils.DTOs.RecordSummaryDTO;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class PayableSeedMapper {

	private final ObjectMapper objMapper;
	
	public List<PayableSeedRequestDTO> jsonToObject(File file) throws Exception, FileNotFoundException {
		if (file == null || !file.exists()) throw new FileNotFoundException();
		String jsonExp = new String();
		Random rand = new Random();
		try {
			var utcDate = new Date(Instant.now().toEpochMilli());
			JsonNode payableNodes = objMapper.readValue(file, JsonNode.class);
			List<PayableSeedRequestDTO> result = new ArrayList<>();
			for(JsonNode payableNode : payableNodes) {
				result.add(PayableSeedRequestDTO.builder()
						.person(getPerson(payableNode))
						.records(getRecord(payableNode))
						.build());
			}
			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	private Person getPerson(JsonNode node) {
		JsonNode personNode = Optional.ofNullable(node)
				.map(n -> n.get("person"))
				.orElseThrow(() -> new IllegalArgumentException("Invalid JSON Object"));
		return Person.builder()
				.name(personNode.get("name").asText())
				.phoneNumber(personNode.get("phoneNumber").asText())
				.email(personNode.get("email").asText())
				.address(personNode.get("address").asText())
				.build();
	}
	
	private List<RecordSummaryDTO> getRecord(JsonNode node){
		JsonNode dtoNodes = Optional.ofNullable(node)
				.map(n -> n.get("records"))
				.orElseThrow(() -> new IllegalArgumentException("Invalid JSON Object"));
		List<RecordSummaryDTO> result = new ArrayList<>(15);
		for(JsonNode dtoNode : dtoNodes) {
			result.add(RecordSummaryDTO.builder()
					.recordDate(dtoNode.get("recordDate").asLong())
					.description(dtoNode.get("description").asText())
					.account(dtoNode.get("account").asInt())
					.entry(dtoNode.get("entry").asInt())
					.balance(dtoNode.get("balance").asLong())
					.build());
		}
		
		return result;
	}
	
}
