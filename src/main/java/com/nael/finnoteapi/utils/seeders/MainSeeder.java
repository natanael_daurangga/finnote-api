package com.nael.finnoteapi.utils.seeders;

import java.io.File;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.PayableDetail;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.ReceivableDetail;
import com.nael.finnoteapi.entities.Record;
import com.nael.finnoteapi.entities.Role;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.ERole;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.enums.RecordGroup;
import com.nael.finnoteapi.repositories.LedgerRepository;
import com.nael.finnoteapi.repositories.PayableRepository;
import com.nael.finnoteapi.repositories.PersonRepository;
import com.nael.finnoteapi.repositories.ReceivableRepository;
import com.nael.finnoteapi.repositories.RecordRepository;
import com.nael.finnoteapi.repositories.RoleRepository;
import com.nael.finnoteapi.repositories.UserRepository;
import com.nael.finnoteapi.utils.DTOs.RecordSummaryDTO;
import com.nael.finnoteapi.utils.DTOs.PayableSeedRequestDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedRequestDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedResponseDTO;
import com.nael.finnoteapi.utils.seeders.mapper.PayableSeedMapper;
import com.nael.finnoteapi.utils.seeders.mapper.PersonSeedMapper;
import com.nael.finnoteapi.utils.seeders.mapper.ReceivableSeedMapper;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class MainSeeder {
	
	private final RoleRepository roleRepository;
	
	private final UserRepository userRepository;
	
	private final LedgerRepository ledgerRepository;
	
	private final RecordRepository recordRepository;
	
	private final PersonRepository personRepository;
	
	private final ReceivableRepository receivableRepository;
	
	private final PayableRepository payableRepository;
	
	private final PasswordEncoder encoder;
	
	private final PersonSeedMapper personSeedMapper;
	
	private final ReceivableSeedMapper receivableSeedMapper;
	
	private final PayableSeedMapper payableSeedMapper;
	
	private List<User> users;
	
	private List<Person> persons;
	
	@EventListener
	@Transactional
	public void seed(ContextRefreshedEvent event) throws Exception {
		try {
			seedRolesTable();
			seedUsersTable();
			seedDebtorAndReceivable();
			seedReceivablePerDebtor();
			seedPayablePerDebtor();
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	private void seedRolesTable() {
		var admin = Role.builder()
				.name(ERole.ROLE_ADMIN)
				.build();
		admin.setCreatedAt(getUtcNow());
		var user = Role.builder()
				.name(ERole.ROLE_USER)
				.build();
		user.setCreatedAt(getUtcNow());
		List<Role> roles = Arrays.asList(admin, user);
		roleRepository.saveAll(roles);
	}
	
	private void seedUsersTable() {
		var utcDate = new Date(Instant.now().toEpochMilli());
		
		Optional<Role> optRoleAdmin = roleRepository.findByName(ERole.ROLE_ADMIN);
		if(optRoleAdmin.isPresent()) {
			Role roleAdmin = optRoleAdmin.get();
			var userAdmin = User.builder()
					.email("admin@tester.com")
					.fullName("admin")
					.passwordHash(encoder.encode("Tester1234"))
					.roles(Collections.singletonList(roleAdmin))
					.build();
			userAdmin.setCreatedAt(utcDate);
			userAdmin.setUpdatedAt(utcDate);
			
			userRepository.save(userAdmin);
		}
		
		Optional<Role> optRoleUser = roleRepository.findByName(ERole.ROLE_USER);
		
		if(optRoleUser.isPresent()) {
			Role roleUser = optRoleUser.get();
			var userSample1 = User.builder()
					.email("nael@tester.com")
					.fullName("nael")
					.passwordHash(encoder.encode("Tester1234"))
					.roles(Collections.singletonList(roleUser))
					.build();
			userSample1.setCreatedAt(utcDate);
			userSample1.setUpdatedAt(utcDate);
			
			var userSample2 = User.builder()
					.email("epen@tester.com")
					.fullName("epen")
					.passwordHash(encoder.encode("Tester1234"))
					.roles(Collections.singletonList(roleUser))
					.build();
			userSample2.setCreatedAt(utcDate);
			userSample2.setUpdatedAt(utcDate);
			
			var userSample3 = User.builder()
					.email("ida@tester.com")
					.fullName("ida")
					.passwordHash(encoder.encode("Tester1234"))
					.roles(Collections.singletonList(roleUser))
					.build();
			userSample3.setCreatedAt(utcDate);
			userSample3.setUpdatedAt(utcDate);
			this.users = Arrays.asList(userSample1,userSample2,userSample3);
			userRepository.saveAll(this.users);
			
			cashSeederPerUser(10, userSample1);
			cashSeederPerUser(15, userSample2);
			cashSeederPerUser(9, userSample3);
			List<User> tempUsers = new ArrayList<>(3);
			tempUsers.addAll(List.of(userSample1, userSample2, userSample3));
			this.users = tempUsers;
		}
	}
	
	private void seedDebtorAndReceivable() throws Exception {
		try {
			File file = ResourceUtils.getFile("classpath:json/personSeeds.json");
			persons = personSeedMapper.jsonToObject(file, users);
			personRepository.saveAll(persons);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	private void seedReceivablePerDebtor() throws Exception {
		try {
			File file = ResourceUtils.getFile("classpath:json/receivableSeeds.json");
			List<ReceivableSeedRequestDTO> seeds = receivableSeedMapper.jsonToObject(file);
			seeds.stream().forEach(seed -> {
				Person person = seed.getPerson();
				person.setUser(this.users.get(0));
				person.setCreatedAt(getUtcNow());
				personRepository.save(person);
				subSeedReceivableRecord(seed.getRecords(), person);
			});
		} catch (Exception e) {
			throw e;
		}
	}
	
	private void seedPayablePerDebtor() throws Exception {
		try {
			File file = ResourceUtils.getFile("classpath:json/payableSeeds.json");
			List<PayableSeedRequestDTO> seeds = payableSeedMapper.jsonToObject(file);
			seeds.stream().forEach(seed -> {
				Person person = seed.getPerson();
				person.setUser(this.users.get(0));
				person.setCreatedAt(getUtcNow());
				personRepository.save(person);
				subSeedPayableRecord(seed.getRecords(), person);
			});
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	private void subSeedPayableRecord(List<RecordSummaryDTO> dtos, Person person) {
		dtos.stream().forEach(dto -> {
			Record recordObj = Record.builder()
					.recordDate(new Date(dto.getRecordDate()))
					.user(this.users.get(0))
					.recordGroup(RecordGroup.NEW_PAYABLE)
					.description(dto.getDescription())
					.build();
			recordObj.setCreatedAt(getUtcNow());
			recordRepository.save(recordObj);
			
			PayableDetail detail = PayableDetail.builder()
					.record(recordObj)
					.creditor(person)
					.build();
			detail.setCreatedAt(getUtcNow());
			payableRepository.save(detail);
			
			Ledger ledger = Ledger.builder()
					.record(recordObj)
					.account(Account.PAYABLE)
					.entry(dto.getEntry() == 1 ? Entry.DEBIT : Entry.CREDIT)
					.balance(dto.getBalance())
					.build();
			ledger.setCreatedAt(getUtcNow());
			ledgerRepository.save(ledger);
		});
	}
	
	private void subSeedReceivableRecord(List<RecordSummaryDTO> dtos, Person person) {
		dtos.stream().forEach(dto -> {
			Record recordObj = Record.builder()
					.recordDate(new Date(dto.getRecordDate()))
					.user(this.users.get(0))
					.recordGroup(RecordGroup.NEW_RECEIVABLE)
					.description(dto.getDescription())
					.build();
			recordObj.setCreatedAt(getUtcNow());
			recordRepository.save(recordObj);
			
			ReceivableDetail detail = ReceivableDetail.builder()
					.record(recordObj)
					.debtor(person)
					.build();
			detail.setCreatedAt(getUtcNow());
			receivableRepository.save(detail);
			
			Ledger ledger = Ledger.builder()
					.record(recordObj)
					.account(Account.RECEIVABLE)
					.entry(dto.getEntry() == 1 ? Entry.DEBIT : Entry.CREDIT)
					.balance(dto.getBalance())
					.build();
			ledger.setCreatedAt(getUtcNow());
			ledgerRepository.save(ledger);
		});
	}
	
//	TODO: Bikin seeder untuk transaksi receivable
	private void cashSeederPerUser(int length, User user) {
		if(length == 0) return;
		List<Record> records = new ArrayList<>(length);
		List<Ledger> ledgers = new ArrayList<>(length);
		
		Random randIndex = new Random();
		Long randBalance = 0l;
		RecordGroup cashFlow = RecordGroup.CASH_IN;
		Long tempTotalBalance = 0l;
		Record randRecord = new Record();
		Ledger randLedger = new Ledger();
		
		for(int i = 0 ; i < length ; i++) {
			cashFlow = i % 2 == 0 ? RecordGroup.CASH_OUT : RecordGroup.CASH_IN;
			randRecord = Record.builder()
					.recordDate(new Date())
					.description("User : " + user.getFullName())
					.recordGroup(cashFlow)
					.user(user)
					.build();
			randRecord.setCreatedAt(new Date());
			records.add(randRecord);
			
			randBalance = getRandomNumber(50, 150) * 1000l;
			tempTotalBalance = sumTotalBalance(ledgers);
			if(randBalance > tempTotalBalance && cashFlow == RecordGroup.CASH_OUT) {
				randBalance -= (randBalance - tempTotalBalance);
			}
			
			randLedger = Ledger.builder()
					.account(Account.CASH)
					.record(randRecord)
					.entry(cashFlow == RecordGroup.CASH_IN ? Entry.DEBIT : Entry.CREDIT)
					.balance(randBalance)
					.build();
			
			randLedger.setCreatedAt(new Date());
			ledgers.add(randLedger);
		}
		
		recordRepository.saveAll(records);
		ledgerRepository.saveAll(ledgers);
	}
	
	public static Long sumTotalBalance(List<Ledger> ledgers) {
		if(ledgers.size() == 0) return 0l;
		return ledgers.stream().mapToLong(obj -> {
			int vector = obj.getEntry() == Entry.DEBIT ? 1 : -1;
			return vector * obj.getBalance();
		}).sum();
	}
	
	public int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}
	
	private Date getUtcNow() {
		return new Date(Instant.now().toEpochMilli());
	}
}
