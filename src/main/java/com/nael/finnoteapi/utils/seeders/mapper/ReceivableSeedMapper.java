package com.nael.finnoteapi.utils.seeders.mapper;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.nael.finnoteapi.entities.Ledger;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.entities.ReceivableDetail;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.enums.RecordGroup;
import com.nael.finnoteapi.utils.DTOs.RecordSummaryDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedRequestDTO;
import com.nael.finnoteapi.utils.DTOs.ReceivableSeedResponseDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Component
@RequiredArgsConstructor
public class ReceivableSeedMapper {
	
	private final ObjectMapper objMapper;
	
	public List<ReceivableSeedRequestDTO> jsonToObject(File file) throws Exception, FileNotFoundException {
		if(file == null || !file.exists()) throw new FileNotFoundException();
		String jsonExp = new String(Files.readAllBytes(file.toPath()));
		Random rand = new Random();
		try {
			var utcDate = new Date(Instant.now().toEpochMilli());
			JsonNode receivableNodes = objMapper.readValue(file, JsonNode.class);
			List<ReceivableSeedRequestDTO> result = new ArrayList<>();
			for(JsonNode receivableNode : receivableNodes) {
				result.add(ReceivableSeedRequestDTO.builder()
						.person(getPerson(receivableNode))
						.records(getRecord(receivableNode))
						.build());
			}
			return result;
		} catch (Exception e) {
			throw e;
		}
	}
	
	private Person getPerson(JsonNode node) {
		JsonNode personNode = Optional.ofNullable(node)
				.map(n -> n.get("person"))
				.orElseThrow(() -> new IllegalArgumentException("Invalid JSON Object"));
		return Person.builder()
				.name(personNode.get("name").asText())
				.phoneNumber(personNode.get("phoneNumber").asText())
				.email(personNode.get("email").asText())
				.address(personNode.get("address").asText())
				.build();
	}

	private List<RecordSummaryDTO> getRecord(JsonNode node){
		JsonNode dtoNodes = Optional.ofNullable(node)
				.map(n -> n.get("records"))
				.orElseThrow(() -> new IllegalArgumentException("Invalid JSON Object"));
		List<RecordSummaryDTO> result = new ArrayList<>(15);
		for(JsonNode dtoNode : dtoNodes) {
			result.add(RecordSummaryDTO.builder()
					.recordDate(dtoNode.get("recordDate").asLong())
					.description(dtoNode.get("description").asText())
					.account(dtoNode.get("account").asInt())
					.entry(dtoNode.get("entry").asInt())
					.balance(dtoNode.get("balance").asLong())
					.build());
		}
		
		return result;
	}
}
