package com.nael.finnoteapi.DTOs.app.person;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RequestPersonCreate {

	@NotBlank
	@Size(min = 0, max = 255, message = "Nama tidak boleh lebih dari 255 karakter.")
	private String name;
	
	@Email(message = "Format email tidak sesuai")
	@Size(min = 0, max = 255, message = "Email tidak boleh lebih dari 255 karakter.")
	private String email;
	
	@Size(min = 0, max = 15, message = "Nomor telepon tidak boleh lebih dari 15 karaketer.")
	@Pattern(regexp = "^[0-9]+$", message="Nomor telepon harus berupa angka dan maksimal 15 karakter.")
	private String phoneNumber;
	
	@Size(min = 0, max = 255, message = "Alamat tidak boleh lebih dari 1000 karakter")
	private String address;

}
