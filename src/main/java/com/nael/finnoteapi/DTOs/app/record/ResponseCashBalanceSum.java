package com.nael.finnoteapi.DTOs.app.record;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class ResponseCashBalanceSum {

	private String userId;
	
	private Long totalBalance;
	
}
