package com.nael.finnoteapi.DTOs.app.record;

import java.util.Date;

import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.validations.annotations.DateBeforeToday;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Builder
@Getter
@Setter
public class RequestNewReceivable {

	@DateBeforeToday
	private Date transactionDate;
	
	@Size(min = 0, max = 255, message = "Description tidak boleh lebih dari 255 karakter.")
	private String description;
	
	private Entry entry;
	
	@Min(value = 0, message = "Balance cannot be below zero.")
	private long balance;
	
	@Size(min = 36, max = 37, message = "Secure id can't surpass 36 characters")
	private String debtorSecureId;
	
}
