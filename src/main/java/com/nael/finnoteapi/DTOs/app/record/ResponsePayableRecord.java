package com.nael.finnoteapi.DTOs.app.record;

import java.util.Date;

import com.nael.finnoteapi.DTOs.util.SortedFieldAware;
import com.nael.finnoteapi.enums.Entry;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class ResponsePayableRecord implements SortedFieldAware {

	private String creditorSecureId;
	
	private Date recordDate;
	
	private String description;
	
	private Entry entry;
	
	private long balance;

	@Override
	public String getDefaultField() {
		// TODO Auto-generated method stub
		return "recordDate";
	}
	
}
