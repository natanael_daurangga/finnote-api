package com.nael.finnoteapi.DTOs.app.person;

import java.util.Date;

import com.nael.finnoteapi.DTOs.util.SortedFieldAware;
import com.nael.finnoteapi.entities.Person;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
public class ResponsePerson implements SortedFieldAware {
	
	private String secureId;

	private String name;
	
	private String email;
	
	private String phoneNumber;
	
	private String address;
	
	private Date createdAt;
	
	private String ownerSecureId;
	
	
	
	public ResponsePerson() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ResponsePerson(Person person) {
		super();
		this.secureId = person.getSecureId();
		this.name = person.getName();
		this.email = person.getEmail();
		this.phoneNumber = person.getPhoneNumber();
		this.address = person.getAddress();
		this.createdAt = person.getCreatedAt();
		this.ownerSecureId = person.getUser().getSecureId();
	}

	public ResponsePerson(String secureId, String name, String email, String phoneNumber, String address, Date createdAt,
			String ownerSecureId) {
		super();
		this.secureId = secureId;
		this.name = name;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.createdAt = createdAt;
		this.ownerSecureId = ownerSecureId;
	}
	
	public String getSecureId() {
		return secureId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getOwnerSecureId() {
		return ownerSecureId;
	}

	public void setOwnerSecureId(String ownerSecureId) {
		this.ownerSecureId = ownerSecureId;
	}

	@Override
	public String getDefaultField() {
		return "name";
	}
	
	
	
}
