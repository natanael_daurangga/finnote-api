package com.nael.finnoteapi.DTOs.app.record;

import java.util.Date;

import com.nael.finnoteapi.enums.Entry;
import com.nael.finnoteapi.validations.annotations.DateBeforeToday;

import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestNewCashRecord {

	@DateBeforeToday
	private Date transactionDate;
	
	@Size(min = 0, max = 255, message = "Description tidak boleh lebih dari 255 karakter.")
	private String description;
	
	private Entry entry;
	
	private long balance;
	
}
