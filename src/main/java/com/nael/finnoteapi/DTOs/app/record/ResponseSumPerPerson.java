package com.nael.finnoteapi.DTOs.app.record;

import com.nael.finnoteapi.DTOs.util.SortedFieldAware;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@Builder
public class ResponseSumPerPerson implements SortedFieldAware {

	private String name;
	
	private String secureId;
	
	private long balanceSum;

	@Override
	public String getDefaultField() {
		// TODO Auto-generated method stub
		return "name";
	}
	
}
