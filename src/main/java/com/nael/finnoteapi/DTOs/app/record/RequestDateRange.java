package com.nael.finnoteapi.DTOs.app.record;

import java.util.Date;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@Builder
public class RequestDateRange {
	
	@NotNull
	private Date startDate;
	
	@NotNull
	private Date endDate;
	
}
