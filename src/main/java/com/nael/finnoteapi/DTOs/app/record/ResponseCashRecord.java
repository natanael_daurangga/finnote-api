package com.nael.finnoteapi.DTOs.app.record;

import java.util.Date;

import com.nael.finnoteapi.DTOs.util.SortedFieldAware;
import com.nael.finnoteapi.enums.Entry;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class ResponseCashRecord implements SortedFieldAware {

	private String secureId;
	
	private Date transactionDate;
	
	private String description;
	
	private Entry entry;
	
	private long Balance;

	@Override
	public String getDefaultField() {
		// TODO Auto-generated method stub
		return "transactionDate";
	}
	
}
