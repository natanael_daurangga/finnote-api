package com.nael.finnoteapi.DTOs.pagination;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class ResponsePagination<T> implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8743639270556224851L;

	private List<T> result;
	
	private Integer pages;
	
	private Long elements;
	
	private String keyword;
	
	private Integer currentPages;
	
}
