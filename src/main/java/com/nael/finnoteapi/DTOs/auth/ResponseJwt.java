package com.nael.finnoteapi.DTOs.auth;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ResponseJwt {

	private String token;
	
	private String type;
	
	private String secureId;
	
	private String email;
	
	private List<String> roles;
	
}
