package com.nael.finnoteapi.DTOs.auth;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestLogin {

	@NotBlank(message = "Email wajib diisi.")
	@NotNull(message = "Email wajib diisi.")
	@Size(min = 0, max = 255, message = "Maksimal panjang adalah 255 karakter.")
	@Email(message = "Format alamat email tidak sesuai.")
	private String email;
	
	@NotBlank(message = "Password wajib diisi.")
	@NotNull(message = "Password wajib diisi.")
	@Size(min = 0, max = 255, message = "Jumlah karakter maksimal untuk password adalah 255 karakter.")
	private String password;
	
}
