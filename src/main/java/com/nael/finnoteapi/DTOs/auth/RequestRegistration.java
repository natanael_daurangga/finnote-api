package com.nael.finnoteapi.DTOs.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.nael.finnoteapi.validations.annotations.UniqueEmail;

import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestRegistration {
	
	@Max(value = 255, message = "Maksimal panjang untuk fullName adalah 255 karakter")
	@NotBlank
	private String fullName;
	
	@Max(value = 255, message = "Maksimal panjang untuk fullName adalah 255 karakter")
	@Email(message = "Format email tidak sesuai")
	@UniqueEmail
	@NotBlank
	private String email;
	
	@Max(value = 255, message = "Maksimal panjang untuk password adalah 255 karakter")
	@NotBlank
	private String password;
	
	@NotBlank
	private String repeatPassword;
	
	@AssertTrue(message = "Input ulang password tidak sesuai")
	@JsonIgnore
	public boolean isPasswordMatching() {
		return password != null && password.equals(repeatPassword);
	}
	
}
