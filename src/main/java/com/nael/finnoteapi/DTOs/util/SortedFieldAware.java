package com.nael.finnoteapi.DTOs.util;

public interface SortedFieldAware {

	public String getDefaultField();
	
}
