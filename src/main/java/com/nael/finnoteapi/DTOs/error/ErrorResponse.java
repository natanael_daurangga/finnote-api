package com.nael.finnoteapi.DTOs.error;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.validation.FieldError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ErrorResponse {

	private Map<String, String> errors = new HashMap<>();
	
	public void setErrors(List<FieldError> fieldErrors) {
//		errors = fieldErrors.stream()
//				.collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage));
		Map<String, String> result = new HashMap<>();
		fieldErrors.stream().forEach(fieldError -> {
			result.put(fieldError.getField(), fieldError.getDefaultMessage());
		});
		errors = result;
	}
	
}
