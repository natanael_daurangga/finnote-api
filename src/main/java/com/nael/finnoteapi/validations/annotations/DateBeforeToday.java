package com.nael.finnoteapi.validations.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

import com.nael.finnoteapi.validations.DateBeforeTodayValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = DateBeforeTodayValidator.class)
@Documented
public @interface DateBeforeToday {

	String message() default "Date exceeds the specified limit";
	Class<?>[] groups() default {};
	Class<? extends Payload>[] payload() default {};
	
}
