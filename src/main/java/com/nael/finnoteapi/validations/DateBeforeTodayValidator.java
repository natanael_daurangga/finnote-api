package com.nael.finnoteapi.validations;

import java.util.Date;

import com.nael.finnoteapi.validations.annotations.DateBeforeToday;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class DateBeforeTodayValidator implements ConstraintValidator<DateBeforeToday, Date> {
	
	
	@Override
	public void initialize(DateBeforeToday constraintAnnotation) {
		// TODO Auto-generated method stub
		ConstraintValidator.super.initialize(constraintAnnotation);
	}

	@Override
	public boolean isValid(Date input, ConstraintValidatorContext arg1) {
		// TODO Auto-generated method stub
		if (input == null ) return true;
		try {
			long inputMillis = input.getTime();
			long currMillis = (new Date()).getTime();
			return currMillis >= inputMillis;
		} catch(Exception e) {
			return true;
		}
	}

	

	
	
}
