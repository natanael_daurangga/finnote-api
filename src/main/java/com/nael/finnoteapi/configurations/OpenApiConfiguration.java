package com.nael.finnoteapi.configurations;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.servers.Server;


@Configuration
@OpenAPIDefinition(
		  info =@Info(
		    title = "Finnote API",
		    version = "1.0-SNAPSHOT",
		    contact = @Contact(
		      name = "Nael", email = "nael.dev@tester.com", url = "http://www.nael-dev.tester"
		    ),
		    license = @License(
		      name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0"
		    ),
		    description = "Financial app using spring boot"
		  ),
		  servers = @Server(
		    url = "${nael.app.openapi.dev-url}",
		    description = "Dev"
		  )
		)
@SecurityScheme(
		name = "BearerAuth",
		type = SecuritySchemeType.HTTP, 
		bearerFormat = "JWT",
		scheme = "bearer")
public class OpenApiConfiguration {
	
}
