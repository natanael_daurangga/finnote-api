package com.nael.finnoteapi.configurations;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import com.nael.finnoteapi.utils.JwtUtils;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AuthTokenFilter extends OncePerRequestFilter {
	
	@Autowired
	private JwtUtils jwtUtils;
	
	@Autowired
	private UserDetailsService userDetailService;
	
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		log.info("request");
		try {
			String jwt = parseJwt(request);
//			TODO: bikin customer UsernamePasswordAuthentication yg bisa menampung secureId, bukan hanya password dan username saja
			if(jwt != null && jwtUtils.validateJwtToken(jwt)) {
				UsernamePasswordAuthenticationToken authentication = jwtUtils.getAuthenticationFromToken(jwt);
				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
			
			log.info("jwt berhasil di validasi");
		} catch (Exception e) {
			// TODO: Lanjut bikin email verification sama forgot password
			log.error("Cannot set user authentication: {}", e);
		}
		
		filterChain.doFilter(request, response);
	}
	
	private String parseJwt(HttpServletRequest request) {
		String headerAuth = request.getHeader("Authorization");
		log.info("headerAuth: {}", headerAuth);
		if(StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) { 
			log.info("Parse JWT return {}", headerAuth.substring(7));
			return headerAuth.substring(7);
		}
		log.info("Parse JWT return null");
		return null;
	}

	
	
}
