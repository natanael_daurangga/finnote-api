package com.nael.finnoteapi.configurations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.nael.finnoteapi.entities.Role;
import com.nael.finnoteapi.entities.User;
import com.nael.finnoteapi.repositories.UserRepository;
import com.nael.finnoteapi.services.UserDetailsImpl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
@Component
public class AppUserAuthenticationManager implements AuthenticationManager {

	private final UserRepository userRepository;
	
	private final PasswordEncoder passwordEncoder;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		log.info("Masuk ke authenticate");
		String email = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		Optional<User> optUser = userRepository.findByEmail(email);
		
		if(optUser.isEmpty()) {
			log.info("User tidak ditemukan");
			throw new BadCredentialsException("1000");
		}
		
		User user = optUser.get();
		
		if(!passwordEncoder.matches(password, user.getPasswordHash())) {
			log.info("Password tidak sesuai");
			throw new BadCredentialsException("1000");
		}
		
		log.info("Login berhasil!!!");
		
		UserDetailsImpl userDetails = new UserDetailsImpl(user);
//		log.info(userDetails.getSecureId());

		Authentication auth = new UsernamePasswordAuthenticationToken(userDetails, password, userDetails.getAuthorities());
		return auth;
	}
}
