package com.nael.finnoteapi.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class UtilityConfiguration {

	
	@Bean
	public ObjectMapper objectMapperBean() {
		log.info("objectMapperBean created");
		return new ObjectMapper();
	}
	
}
