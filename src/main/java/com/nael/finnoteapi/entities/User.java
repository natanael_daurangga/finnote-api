package com.nael.finnoteapi.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "users", 
	uniqueConstraints = {
			@UniqueConstraint(columnNames = "email")
	},
	indexes = {@Index(columnList = "secure_id", unique=true, name = "idx_secure_Id")})
@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User extends BaseEntity {
	/**
	 * 
	 */
	private static final long serialVersionUID = 423045243669318323L;

	// TODO: Lanjut bikin dto > service > logic untuk pengembangan lebih lanjut
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "full_name", nullable = false, length = 255)
	private String fullName;
	
	@Column(name = "email", nullable = false, length = 100, unique = true)
	private String email;
	
	@JsonIgnore
	@Column(name = "password_hash", nullable = false, length = 128)
	private String passwordHash;
	
	@Column(name = "profile_picture", length = 255, nullable = true)
	private String profilePicture;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
		  name = "users_roles"
		, joinColumns = @JoinColumn(name = "user_id")
		, inverseJoinColumns = @JoinColumn(name = "role_id")
	)
	private List<Role> roles = new ArrayList<>(5);
	
	@Column(name = "verification_token", length = 128)
	private String verificationToken;
	
	@Column(name = "verified_at")
	private Date verfiedAt;
	
	@Column(name = "verification_expired_at")
	private Date verficationExpiredAt;
	
	@Column(name = "reset_password_token", length = 128)
	private String resetPasswordToken;
	
	@Column(name = "reset_token_expires")
	private Date resetTokenExpires;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
	@JsonManagedReference
	private List<Record> records = new ArrayList<>();

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(User.builder()
					.id(this.id)
					.fullName(this.fullName)
					.email(this.email)
					.build());
		} catch (Exception e) {
			// TODO: handle exception
			return "error";
		}
	}

	
	
}
