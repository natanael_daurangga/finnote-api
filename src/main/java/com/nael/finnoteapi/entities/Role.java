package com.nael.finnoteapi.entities;

import java.io.Serializable;

import com.nael.finnoteapi.enums.ERole;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "roles",
indexes = {@Index(columnList = "secure_id", unique=true, name = "idx_secure_Id")})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Role extends BaseEntity implements Serializable {/**
	 * 
	 */
	private static final long serialVersionUID = 3829212041329270471L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "name", length = 50)
	private ERole name;
	
}
