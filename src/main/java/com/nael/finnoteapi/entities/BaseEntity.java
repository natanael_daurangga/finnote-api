package com.nael.finnoteapi.entities;

import java.io.Serializable;
import java.time.Instant;
import java.util.Date;
import java.util.UUID;

import jakarta.persistence.Column;
import jakarta.persistence.Index;
import jakarta.persistence.MappedSuperclass;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;


@MappedSuperclass
@Getter
@Setter
public abstract class BaseEntity implements Serializable {

	/**
	 * 
	 */
//	TODO: Lanjut bikin entity buat aplikasi cash ini
	private static final long serialVersionUID = -7588053183117255750L;
	
	@Column(name = "secure_id", nullable = false, unique = true, length = 37)
	private String secureId = UUID.randomUUID().toString();
	
	@Column(name = "created_at")
	private Date createdAt;
	
	@Column(name = "updated_at")
	private Date updatedAt = new Date(Instant.now().toEpochMilli());
	
	@Column(name = "deleted_at", nullable = true)
	private Date deletedAt;
	
}
