package com.nael.finnoteapi.entities;

import com.nael.finnoteapi.enums.Account;
import com.nael.finnoteapi.enums.Entry;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "ledgers",
	indexes = {
			@Index(columnList = "secure_id", unique=true, name = "idx_ledger_secure_Id"),
			@Index(columnList = "record_id", name = "idx_ledger_record_id")
})
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Ledger extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long id;

	@ManyToOne
	@JoinColumn(name = "record_id")
	public Record record;
	
	@Column(name = "account")
	public Account account;
	
	@Column(name = "entry")
	public Entry entry;
	
	@Column(name = "balance")
	public Long balance = 0l;
	
	
}
