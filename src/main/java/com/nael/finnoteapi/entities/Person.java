package com.nael.finnoteapi.entities;

import org.hibernate.annotations.SQLDelete;

import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "persons",
		indexes = {
				@Index(columnList = "secure_id", unique=true, name = "idx_person_secure_Id"),
				@Index(columnList = "user_id", name = "idx_person_user_id")
})
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Person extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "user_id")
	private User user;
	
	@Column(name = "name", nullable = false, length = 255)
	private String name;
	
	@Column(name = "phone_number", length = 15)
	private String phoneNumber;
	
	@Column(name = "email", length = 255)
	private String email;
	
	@Column(name = "address", length = 255)
	private String address;

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(Person.builder()
					.id(this.id)
					.user(this.user)
					.name(this.name)
					.phoneNumber(this.phoneNumber)
					.email(this.email)
					.address(this.address)
					.build());
		} catch (Exception e) {
			// TODO: handle exception
			return "error";
		}
	}
	
	
	
}
