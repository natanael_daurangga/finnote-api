package com.nael.finnoteapi.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Index;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "receivable_detail",
indexes = {@Index(columnList = "secure_id", unique=true, name = "idx_secure_Id")})
@Setter
@Getter
@Builder
public class ReceivableDetail extends BaseEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@OneToOne
	@JoinColumn(name = "record_id")
	private Record record;
	
	@ManyToOne
	@JoinColumn(name = "person_id")
	private Person debtor;

}
