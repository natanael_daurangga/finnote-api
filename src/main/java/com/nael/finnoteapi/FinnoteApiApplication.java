package com.nael.finnoteapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinnoteApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinnoteApiApplication.class, args);
	}

}
