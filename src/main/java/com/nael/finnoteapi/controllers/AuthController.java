package com.nael.finnoteapi.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import jakarta.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nael.finnoteapi.DTOs.auth.RequestLogin;
import com.nael.finnoteapi.DTOs.auth.RequestRegistration;
import com.nael.finnoteapi.DTOs.auth.ResponseJwt;
import com.nael.finnoteapi.services.AuthService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

	@Autowired
	AuthService authService;
	
	@PostMapping("register")
	public ResponseEntity<?> register(@Valid @RequestBody RequestRegistration request, BindingResult bindingResult) {
		
		try {
			if(bindingResult.hasErrors()) {
				List<String> errorMessages = bindingResult.getAllErrors()
						.stream()
						.map(error -> error.getDefaultMessage())
						.collect(Collectors.toList());
				
				return ResponseEntity.badRequest().body(String.join(",", errorMessages));
			}
			
			boolean result = authService.registerUser(request);
			if(result) return ResponseEntity.ok("Data user berhasil diregistrasi.");
			
			return ResponseEntity.badRequest().body("Data user gagal diregistrasi.");			
		} catch (Exception e) {
//			return ResponseEntity.internalServerError().body("Internal error has been occured.");
			throw e;
		}
	}
	
	@PostMapping("login")
	public ResponseEntity<?> login(@Valid @RequestBody RequestLogin request, BindingResult bindingResult) {
		log.info("endpoint login (/login)");
		if(bindingResult.hasErrors()) {
			List<String> errorMessages = bindingResult.getAllErrors()
					.stream()
					.map(error -> error.getDefaultMessage())
					.collect(Collectors.toList());
			
			return ResponseEntity.badRequest().body(String.join(",", errorMessages));
		}
		
		ResponseJwt responseJwt = authService.authenticate(request);
		return ResponseEntity.ok(responseJwt);
	}
	
}
