package com.nael.finnoteapi.controllers;

import java.util.Date;

import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nael.finnoteapi.DTOs.app.record.RequestNewPayable;
import com.nael.finnoteapi.DTOs.app.record.ResponsePayableBalanceSum;
import com.nael.finnoteapi.DTOs.app.record.ResponseReceivableBalanceSum;
import com.nael.finnoteapi.DTOs.app.record.ResponseRecordSummary;
import com.nael.finnoteapi.DTOs.app.record.ResponseSumPerPerson;
import com.nael.finnoteapi.DTOs.error.ErrorResponse;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.services.PayableService;
import com.nael.finnoteapi.services.ReceivableService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/payable")
@RequiredArgsConstructor
public class PayableController {

	private final PayableService payableService;
	
	@PostMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menambahkan catatan utang baru", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> insertNewPayable(@Valid @RequestBody RequestNewPayable dto, BindingResult bindingResult) {
		try {
			if(bindingResult.hasErrors()) {
				var errorResponse = new ErrorResponse();
				errorResponse.setErrors(bindingResult.getFieldErrors());
				return ResponseEntity.badRequest().body(errorResponse.getErrors());
			}
			payableService.saveNewRecord(dto);
			return ResponseEntity.ok("Data berhasil disimpan");
		} catch (Exception e) {
			 throw e;
		}
	}
	
	@GetMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Daftar transaksi payable per creditor", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> paginatePayableRecord(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "limit", defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", defaultValue = "p.name") String sortBy,
			@RequestParam(name = "direction", defaultValue = "asc") String direction,
			@RequestParam(name = "keyword", defaultValue = "") String keyword,
			@RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date endDate
			){
		try {
			ResponsePagination<ResponseSumPerPerson> result = payableService
					.paginate(page, limit, sortBy, direction, keyword, startDate, endDate);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			throw e;
		}
	}
	
	@GetMapping("detail")
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Daftar saldo payable per creditor", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> paginatePayableDetail(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "limit", defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", defaultValue = "recordDate") String sortBy,
			@RequestParam(name = "direction", defaultValue = "asc") String direction,
			@RequestParam(name = "keyword", defaultValue = "") String keyword,
			@RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date endDate,
			@RequestParam(name = "debtorSecureId", required = true, defaultValue = "") String debtorSecureId 
			){
		try {
			ResponsePagination<ResponseRecordSummary> result = payableService
					.paginateDetail(page, limit, sortBy, direction, keyword, startDate, endDate, debtorSecureId);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@GetMapping("{creditorSecureId}")
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Sum utang per creditor", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> sumPayableRecord(@PathVariable("creditorSecureId") String secureId) {
		try {
			ResponsePayableBalanceSum result = payableService.payableBalanceSum(secureId);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			throw e;
		}
	}
	
	
	
}
