package com.nael.finnoteapi.controllers;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nael.finnoteapi.DTOs.app.record.RequestNewCashRecord;
import com.nael.finnoteapi.DTOs.app.record.ResponseCashRecord;
import com.nael.finnoteapi.DTOs.error.ErrorResponse;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.services.CashService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v1/cash")
public class CashController {

	private final CashService cashService;
	
	@Autowired
	public CashController(CashService cashService) {
		super();
		this.cashService = cashService;
	}
	
	@GetMapping("/sum")
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Mentotalkan saldo kas tiap user", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> getTotalCashBalance() {
		try {
			return ResponseEntity.ok(cashService.sumCashBalancePerUser());
		} catch (Exception e) {
			// TODO: handle exception
			throw e;
		}
	}
	
	@PostMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menambahkan catatan kas baru", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> insertNewCashRecord(@Valid @RequestBody RequestNewCashRecord dto, BindingResult bindingResult) {
		try {
//			FIXME: dapet error date time exceed limit, meskipun inputnya sebelum hari ini
			if(bindingResult.hasErrors()) {
				log.info("error ditangkap, jumlah: {}", bindingResult.getFieldErrors().size());
				var errorResponse = new ErrorResponse();
				errorResponse.setErrors(bindingResult.getFieldErrors());
				return ResponseEntity.badRequest().body(errorResponse.getErrors());
			}
			
			cashService.insertInRecord(dto);
			return ResponseEntity.ok("Data berhasil disimpan");
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}
//	TODO: Lanjut bikin soft delete untuk record cash
//	public ResponseEntity<?> pag
	
	@GetMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Daftar transaksi kas per user", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> paginateCashRecord(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "limit", defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", defaultValue = "recordDate") String sortBy,
			@RequestParam(name = "direction", defaultValue = "asc") String direction,
			@RequestParam(name = "keyword", defaultValue = "") String keyword,
			@RequestParam(name = "startDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date startDate,
			@RequestParam(name = "endDate", required = false) @DateTimeFormat(iso = ISO.DATE) Date endDate
	){
		try {
			ResponsePagination<ResponseCashRecord> result = cashService
					.findAllCashRecord(page, limit, sortBy, direction, keyword, startDate, endDate);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			throw e;
		}
	}
}
