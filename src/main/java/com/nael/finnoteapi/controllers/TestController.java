package com.nael.finnoteapi.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/test")
@SecurityRequirement(name = "bearerAuth")
public class TestController {

	@GetMapping("filterUser")
	@Operation(summary = "filterUser endpoint", security = @SecurityRequirement(name = "BearerAuth"))
	@PreAuthorize("hasRole('ROLE_USER')")
	public ResponseEntity<?> filterUser(){
		log.info("filterUser endpoint");
		return ResponseEntity.ok("Hello user...");
	}
	
	@GetMapping("filterAdmin")
	@Operation(summary = "filterAdmin endpoint", security = @SecurityRequirement(name = "BearerAuth"))
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> filterAdmin(){
		return ResponseEntity.ok("Hello admin...");
	}
	
}
