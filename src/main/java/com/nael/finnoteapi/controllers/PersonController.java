package com.nael.finnoteapi.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.nael.finnoteapi.DTOs.app.person.RequestPersonCreate;
import com.nael.finnoteapi.DTOs.app.person.ResponsePerson;
import com.nael.finnoteapi.DTOs.error.ErrorResponse;
import com.nael.finnoteapi.DTOs.pagination.ResponsePagination;
import com.nael.finnoteapi.entities.Person;
import com.nael.finnoteapi.services.PersonService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.persistence.EntityNotFoundException;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("api/v1/person")
public class PersonController {

	private final PersonService personService;
	
	@Autowired
	public PersonController(PersonService personService) {
		super();
		this.personService = personService;
	}
	
	@PostMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menambahkan contact person baru", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> insertNewPerson(@Valid @RequestBody RequestPersonCreate dto, BindingResult bindingResult){
		try {
			if(bindingResult.hasErrors()) {
				log.info("error ditangkap, jumlah: {}", bindingResult.getFieldErrors().size());
				var errorResponse = new ErrorResponse();
				errorResponse.setErrors(bindingResult.getFieldErrors());
				return ResponseEntity.badRequest().body(errorResponse.getErrors());
			}
			
			personService.insertNewPerson(dto);
			
			return ResponseEntity.ok("Successfully saved");
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}
	
//	TODO: bikin GET data person
	@DeleteMapping("/{secure_id}")
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menghapus data contact person", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> deletePerson(@PathVariable("secure_id") String secureId){
		try {
			personService.softDeletePerson(secureId);
			return ResponseEntity.ok("Data berhasil dihapus");
		} catch (EntityNotFoundException e) {
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}
	
	@GetMapping
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menghapus data contact person", security = @SecurityRequirement(name = "BearerAuth"))	
	public ResponseEntity<?> getPersonData(
			@RequestParam(name = "page", defaultValue = "0") Integer page,
			@RequestParam(name = "limit", defaultValue = "10") Integer limit,
			@RequestParam(name = "sortBy", defaultValue = "name") String sortBy,
			@RequestParam(name = "direction", defaultValue = "asc") String direction,
			@RequestParam(name = "keyword", defaultValue = "") String keyword
			){
		try {
			ResponsePagination<ResponsePerson> result = personService.findAllPerson(page, limit, sortBy, direction, keyword);
			return ResponseEntity.ok(result);
		} catch (Exception e) {
			return ResponseEntity.internalServerError().build();
		}
	}
	
//	TODO: test dulu get person ini, lalu lanjut ke transaksi kas
	@GetMapping("/{secureId}")
	@PreAuthorize("hasRole('USER')")
	@Operation(summary = "Menghapus data contact person", security = @SecurityRequirement(name = "BearerAuth"))
	public ResponseEntity<?> getPerson(@PathVariable("secureId") String secureId) {
		try {
			var result = personService.findPerson(secureId);
			return ResponseEntity.ok(result);
		} catch (EntityNotFoundException e) {
			return ResponseEntity.notFound().build();
		} catch (Exception e) {
			// TODO: handle exception
			return ResponseEntity.internalServerError().build();
		}
	}
	
	
}
