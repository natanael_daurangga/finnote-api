package com.nael.finnoteapi.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nael.finnoteapi.DTOs.app.record.ResponseRecordSummary;
import com.nael.finnoteapi.DTOs.app.record.ResponseSumPerPerson;
import com.nael.finnoteapi.entities.ReceivableDetail;

public interface ReceivableRepository extends JpaRepository<ReceivableDetail, Long> {

	@Query("SELECT new com.nael.finnoteapi.DTOs.app.record.ResponseRecordSummary("
			+ "p.secureId, r.recordDate, r.description, l.entry, l.balance"
			+ ") "
			+ "FROM Record r "
			+ "INNER JOIN ReceivableDetail rc ON r.id = rc.record.id "
			+ "INNER JOIN Person p ON p.id = rc.debtor.id "
			+ "INNER JOIN Ledger l ON l.record.id = r.id "
			+ "WHERE r.user.id = :userId "
			+ "AND r.deletedAt IS NULL "
			+ "AND p.secureId = :debtorSecureId "
			+ "AND (r.recordDate BETWEEN :startDate AND :endDate)")
	Page<ResponseRecordSummary> getReceivableTransactionDetail(
				@Param("userId") Long userId,
				@Param("debtorSecureId") String debtorSecureId,
				@Param("startDate") Date startDate,
				@Param("endDate") Date endDate,
				Pageable pageable
			);

	@Query("SELECT SUM("
			+ "CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END "
			+ ") "
			+ "FROM Record r "
			+ "INNER JOIN ReceivableDetail rc ON r.id = rc.record.id "
			+ "INNER JOIN Person p ON p.id = rc.debtor.id "
			+ "INNER JOIN Ledger l ON l.record.id = r.id "
			+ "WHERE (r.deletedAt IS NULL) "
			+ "AND (r.user.id = :userId) "
			+ "AND (p.secureId = :debtorSecureId)")
	Long sumReceivablePerDebtor(@Param("userId") Long userId, @Param("debtorSecureId") String debtorSecureId);

	@Query("SELECT "
			+ "new com.nael.finnoteapi.DTOs.app.record.ResponseSumPerPerson("
			+ "	p.name, "
			+ " p.secureId, "
			+ " SUM(CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END)"
			+ ") "
			+ "FROM Person p "
			+ "INNER JOIN ReceivableDetail rc ON p.id = rc.debtor.id "
			+ "INNER JOIN Record r ON r.id = rc.record.id "
			+ "LEFT JOIN Ledger l ON l.record.id = r.id "
			+ "WHERE (r.deletedAt IS NULL) "
			+ "AND (r.user.id = :userId) "
			+ "GROUP BY p.name, p.secureId")
	Page<ResponseSumPerPerson> getReceivablePerDebtor(
			@Param("userId") Long userId, 
			Pageable pageable);
	
}
