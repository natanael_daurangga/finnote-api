package com.nael.finnoteapi.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nael.finnoteapi.entities.Record;

public interface RecordRepository extends JpaRepository<Record, Long> {

	Optional<Record> findBySecureId(String secureId);
	
}
