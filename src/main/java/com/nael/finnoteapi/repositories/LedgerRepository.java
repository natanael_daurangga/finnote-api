package com.nael.finnoteapi.repositories;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.nael.finnoteapi.DTOs.app.record.ResponseCashRecord;
import com.nael.finnoteapi.entities.Ledger;

public interface LedgerRepository extends JpaRepository<Ledger, Long> {

//	TODO: Buat pagination yg memiliki range tanggal
	@Query("SELECT new com.nael.finnoteapi.DTOs.app.record.ResponseCashRecord(r.secureId, r.recordDate, r.description, l.entry, l.balance) "
			+ "FROM Record r "
			+ "INNER JOIN Ledger l ON l.record.id = r.id "
			+ "WHERE (r.recordDate BETWEEN :startDate AND :endDate) "
			+ "AND (r.user.id = :userId) "
			+ "AND (r.deletedAt IS NULL)")
	Page<ResponseCashRecord> getCashRecordPageable(
			@Param("userId") Long userId, 
			@Param("startDate") Date startDate, 
			@Param("endDate") Date endDate, 
			Pageable pageable
	);
	
	@Query("SELECT SUM("
			+ "CASE WHEN l.entry = 1 THEN l.balance ELSE -l.balance END "
			+ ") "
			+ "FROM Record r "
			+ "INNER JOIN Ledger l ON l.record.id = r.id "
			+ "WHERE (r.deletedAt IS NULL) "
			+ "AND (r.user.id = :userId)")
	Long sumLedgerBalance(@Param("userId") Long userId);
	
	
	
}
