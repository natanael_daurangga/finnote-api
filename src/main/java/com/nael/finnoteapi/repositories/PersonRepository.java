package com.nael.finnoteapi.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nael.finnoteapi.DTOs.app.person.ResponsePerson;
import com.nael.finnoteapi.entities.Person;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

//	@Query("SELECT p FROM Person p WHERE p.secureId = :secureId AND p.userId = :userId")
//	Optional<Person> findOneBySecureIdAndUserId(@Param("secureId") String secureId, @Param("userId") Integer userId);
	Optional<Person> findOneBySecureIdAndUserId(String secureId, Long userId);
	
	@Query("SELECT new com.nael.finnoteapi.DTOs.app.person.ResponsePerson(p.secureId, p.name, p.email, p.phoneNumber, p.address, p.createdAt, u.secureId) "
			+ "FROM Person p "
			+ "INNER JOIN User u ON u.id = p.user.id "
			+ "WHERE p.user.id = :userId "
			+ "AND (LOWER(p.name) LIKE LOWER(CONCAT('%', :keyword, '%')) "
			+ "OR LOWER(p.email) LIKE LOWER(CONCAT('%', :keyword, '%')) "
			+ "OR LOWER(p.phoneNumber) LIKE LOWER(CONCAT('%', :keyword, '%'))) "
			+ "AND (p.deletedAt IS NULL)")
	Page<ResponsePerson> getPersonsPageable(@Param("userId") Long userId
			,@Param("keyword") String keyword,
			Pageable pageable);
	
}
