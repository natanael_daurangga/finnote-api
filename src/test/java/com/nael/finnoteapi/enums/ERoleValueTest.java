package com.nael.finnoteapi.enums;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import io.jsonwebtoken.lang.Assert;

public class ERoleValueTest {

	@Test
	void contextLoad() {
		String roleName = ERole.ROLE_USER.name();
		
		Assert.isTrue(roleName.equals("ROLE_USER"));
	}
	
}
