package com.nael.finnoteapi.repositories;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import jakarta.activation.DataSource;
import jakarta.persistence.EntityManager;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PersonRepositoryTest {

	@Autowired
	DataSource dataSource;
	
	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	EntityManager entityManager;
	
	@Autowired
	PersonRepository personRepository;
//	
//	
	@Test
	public void isDataExists() {
//		long length = personRepository.count();
//		Assertions.assertTrue(length > 0);
		Assertions.assertNotNull(dataSource);
//		Assertions.assertNotNull(personRepository);
	}
	
}
